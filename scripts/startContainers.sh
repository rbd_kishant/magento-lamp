#!/bin/bash

distro='debian'
codename='stretch'
containerd_VER='1.2.6-3'
docker_VER='19.03.8~3-0'
compose_VER='1.25.4'

curl https://download.docker.com/linux/${distro}/dists/${codename}/pool/stable/amd64/containerd.io_${containerd_VER}_amd64.deb --output containerd.io_${containerd_VER}_amd64.deb
curl https://download.docker.com/linux/${distro}/dists/${codename}/pool/stable/amd64/docker-ce_${docker_VER}~${distro}-${codename}_amd64.deb --output docker-ce_${docker_VER}~${distro}-${codename}_amd64.deb
curl https://download.docker.com/linux/${distro}/dists/${codename}/pool/stable/amd64/docker-ce-cli_${docker_VER}~${distro}-${codename}_amd64.deb --output docker-ce-cli_${docker_VER}~${distro}-${codename}_amd64.deb

dpkg -i containerd.io_${containerd_VER}_amd64.deb
dpkg -i docker-ce-cli_${docker_VER}~${distro}-${codename}_amd64.deb
dpkg -i docker-ce_${docker_VER}~${distro}-${codename}_amd64.deb

curl -L "https://github.com/docker/compose/releases/download/${compose_VER}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

cd ..
docker-compose up -d
